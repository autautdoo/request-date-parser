const periodHandler = require('./handlers/period.js');
const fromToHandler = require('./handlers/fromToDate.js');
const intervalHandler = require('./handlers/interval.js');
const {UnknownTimeSpecificationError} = require('./errors.js');

module.exports = {
  /**
   * Parse dates from request
   *
   * @param {IncomingMessage} req
   * @param {DateParserOptions} options
   * @throws UnknownTimeSpecificationError
   * @returns {[Date, Date][moment, moment]}
   */
  getFromTo(req, options={}) {
    const fromDate = req.query.fromDate || req.body.fromDate || options.fromDate;
    const toDate = req.query.toDate || req.body.toDate || options.toDate;
    const period = (req.query.period || req.body.period || options.period || '').toLowerCase();
    const intervalType = ( // this ensures backwards compatibility with igp-reports app
      req.query.intervalType || req.body.intervalType || options.intervalType || ''
    ).toLowerCase();
    let interval = '';
    if (intervalType && intervalType !== 'custom') {
      interval = req.query[intervalType] || req.body[intervalType];
    }
    const previousPeriod = options.previousPeriod || false;

    if (period) {
      return periodHandler(period, previousPeriod);
    } else if (fromDate || toDate || intervalType === 'custom') {
      return fromToHandler(fromDate, toDate, previousPeriod);
    } else if (intervalType && interval) {
      return intervalHandler(intervalType, interval, previousPeriod);
    } else {
      throw new UnknownTimeSpecificationError({fromDate, toDate, period, interval, intervalType});
    }
  },

  UnknownTimeSpecificationError,
};
