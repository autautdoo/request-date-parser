/**
 * @typedef {Object} DateParserOptions
 * @property {?Date} fromDate - optional default from date
 * @property {?Date} toDate - optional default to date
 * @property {?Date} period - optional default period
 * @property {?boolean} intervalType - kept for compatibility
 * @property {?boolean} previousPeriod - should consider previous period
 */

class UnknownTimeSpecificationError extends Error {
  constructor(data) {
    super();
    this.name = 'UnknownTimeSpecificationError';
    this.message = 'Unknown time specification';
    this.data = data;
  }
}

module.exports = {UnknownTimeSpecificationError};
