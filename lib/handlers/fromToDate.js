const moment = require('moment');
const {UnknownTimeSpecificationError} = require('../errors.js');

module.exports = (fromDate, toDate, previous=false) => {
  if (!fromDate) {
    throw new UnknownTimeSpecificationError({fromDate, toDate});
  }

  const _fromDate = moment.utc(fromDate);
  if (!_fromDate.isValid()) {
    throw new UnknownTimeSpecificationError({fromDate, toDate});
  }
  _fromDate.startOf('day');

  let _toDate;
  if (toDate) {
    _toDate = moment.utc(toDate);
    if (!_toDate.isValid()) {
      _toDate = moment.utc();
    }
    _toDate.endOf('day');
  }

  if (previous) {
    if (!_toDate) {
      _toDate = moment().utc();
    }
    let dateDiffDays = moment.duration(_toDate.diff(_fromDate)).asDays();
    dateDiffDays = Math.ceil(dateDiffDays);
    _fromDate.subtract(dateDiffDays, 'days');
    _toDate.subtract(dateDiffDays, 'days');
  }

  fromDate = _fromDate.toISOString();
  toDate = _toDate ? _toDate.toISOString() : undefined;

  return [fromDate, toDate];
};
