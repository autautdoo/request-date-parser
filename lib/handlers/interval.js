const moment = require('moment');

module.exports = (intervalType, interval, previous=false) => {
  if (intervalType === 'today') {
    intervalType = 'day';
  }

  let fromDate = null;
  let toDate = null;

  switch (intervalType) {
    case 'day':
    case 'week':
    case 'month':
    case 'quarter':
    case 'year':
      fromDate = moment.utc(interval).startOf(intervalType);
      toDate = moment.utc(interval).endOf(intervalType);
      if (previous) {
        fromDate.subtract(1, intervalType).startOf(intervalType);
        toDate.subtract(1, intervalType).endOf(intervalType);
      }
      fromDate = fromDate.toISOString();
      toDate = toDate.toISOString();
      break;
    default:
      fromDate = moment().utc().toISOString(); break;
  }

  return [fromDate, toDate];
};
