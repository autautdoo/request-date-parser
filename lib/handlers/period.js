const moment = require('moment');

module.exports = (period, previous=false) => {
  const periodsToSubtract = previous ? 2 : 1;

  const now = moment.utc();
  let fromDate;
  let toDate;

  /*
  https://autaut.atlassian.net/browse/IGPSUP-1500
  Please implement this way relative data range filter

  “Last n days” - Starts at 00:00:00 (hh:mm:ss) n days before the current day and continues up to the current date and time. The range includes today and using this date value includes records from n + 1 days ago up to the current day. For example:
    If today is 21.09.2020 and it was used “Last 7 days” then data range will be 14.09.2020 00:00:00 to 21.09.2020 23:59:59
    “n to date” - You specify a past Time Level (Year, Quarter, Month, Week) relative to today's date (current date and time - Now). For example:
  “Month to date” retrieves data from 00:00:00 (midnight) of the first day of this month up until the current date and time (that is, Today or Now).
  “n days ago” - Starts at 00:00:00 on the day n days before the current day and continues for 24 hours (the range does not include today). For example:
    If today is 21.09.2020 and it was used  “7 days ago” then data range will be 14.09.2020 - 20.09.2020
  */
  switch (period) {
    case 'all':
      fromDate = moment.utc('2010-01-01');
      toDate = now.endOf('day');
      break;
    case 'today':
    case 'day': // kept for compatibility reasons
      // from 00:00 until now
      fromDate = now.startOf('day').subtract((periodsToSubtract - 1), 'day');
      toDate = fromDate.clone().endOf('day');
      break;
    case 'yesterday':
      // yesterday from 00:00 until the end of day
      fromDate = now.startOf('day').subtract(periodsToSubtract, 'day');
      toDate = fromDate.clone().endOf('day');
      break;
    case 'last24hours':
      // last 24 hours until now
      fromDate = now.clone().startOf('hour').subtract(periodsToSubtract, 'day');
      toDate = now.subtract((periodsToSubtract - 1), 'day').endOf('hour');
      if (previous) {
        toDate.subtract(1, 'hour');
      }
      break;
    case 'last7days':
      // last 7 days until yesterday end of day (7 days in total)
      fromDate = now.startOf('day').subtract(7 * periodsToSubtract, 'day');
      toDate = fromDate.clone().endOf('day').add(7, 'day');
      if (previous) {
        toDate.subtract(1, 'day');
      }
      break;
    case 'last30days':
      // last 30 days until yesterday end of day (30 days in total)
      fromDate = now.startOf('day').subtract(30 * periodsToSubtract, 'day');
      toDate = fromDate.clone().endOf('day').add(30, 'day');
      if (previous) {
        toDate.subtract(1, 'day');
      }
      break;
    case 'last60days':
      // last 60 days until yesterday end of day (60 days in total)
      fromDate = now.startOf('day').subtract(60 * periodsToSubtract, 'day');
      toDate = fromDate.clone().endOf('day').add(60, 'day');
      if (previous) {
        toDate.subtract(1, 'day');
      }
      break;
    case 'last90days':
      // last 90 days until yesterday end of day (90 days in total)
      fromDate = now.startOf('day').subtract(90 * periodsToSubtract, 'day');
      toDate = fromDate.clone().endOf('day').add(90, 'day');
      if (previous) {
        toDate.subtract(1, 'day');
      }
      break;
    case 'last120days':
      // last 120 days until yesterday end of day (120 days in total)
      fromDate = now.startOf('day').subtract(120 * periodsToSubtract, 'day');
      toDate = fromDate.clone().endOf('day').add(120, 'day');
      if (previous) {
        toDate.subtract(1, 'day');
      }
      break;
    case 'last180days':
      // last 180 days until yesterday end of day (180 days in total)
      fromDate = now.startOf('day').subtract(180 * periodsToSubtract, 'day');
      toDate = fromDate.clone().endOf('day').add(180, 'day');
      if (previous) {
        toDate.subtract(1, 'day');
      }
      break;
    case 'weektodate':
      // from 1st of current week until end of current week (week starts from Sunday)
      // if previous=true, toDate is set to end of 'this' day for previous week
      fromDate = now.clone().startOf('week').subtract((periodsToSubtract - 1), 'week');
      toDate = now.subtract((periodsToSubtract - 1), 'week').endOf(previous ? 'day' : 'week');
      break;
    case 'monthtodate':
      // from 1st of current month until end of current month
      // if previous=true, toDate is set to end of 'this' day for previous month
      fromDate = now.clone().startOf('month').subtract((periodsToSubtract - 1), 'month');
      toDate = now.subtract((periodsToSubtract - 1), 'month').endOf(previous ? 'day' : 'month');
      break;
    case 'quartertodate':
      // from 1st of current quarter until end of current quarter
      // if previous=true, toDate is set to end of 'this' day for previous quarter
      fromDate = now.clone().startOf('quarter').subtract((periodsToSubtract - 1), 'quarter');
      toDate = now.subtract((periodsToSubtract - 1), 'quarter').endOf(previous ? 'day' : 'quarter');
      break;
    case 'yeartodate':
      // from 1st of current year until end of current year
      // if previous=true, toDate is set to end of 'this' day for previous year
      fromDate = now.clone().startOf('year').subtract((periodsToSubtract - 1), 'year');
      toDate = now.subtract((periodsToSubtract - 1), 'year').endOf(previous ? 'day' : 'year');
      break;
    case 'previousweek':
      fromDate = now.startOf('week').subtract(periodsToSubtract, 'week');
      toDate = fromDate.clone().add(1, 'week').subtract(1, 'ms');
      break;
    case 'previousmonth':
    case 'lastmonth': // kept for compatibility reasons
      // from 1st to last day of last month
      fromDate = now.startOf('month').subtract(periodsToSubtract, 'month');
      toDate = fromDate.clone().add(1, 'month').subtract(1, 'ms');
      break;
    case 'previousquarter':
      fromDate = now.startOf('quarter').subtract(periodsToSubtract, 'quarter');
      toDate = fromDate.clone().add(1, 'quarter').subtract(1, 'ms');
      break;
    case 'previousyear':
    case 'lastyear': // kept for compatibility reasons
      // from 1.1. to 31.12. of last year
      fromDate = now.startOf('year').subtract(periodsToSubtract, 'year');
      toDate = fromDate.clone().add(1, 'year').subtract(1, 'ms');
      break;
    case '24hoursago':
      fromDate = now.clone().startOf('hour').subtract(periodsToSubtract, 'day');
      toDate = fromDate.clone().add(24, 'hour').subtract(1, 'ms');
      break;
    case '7daysago':
      fromDate = now.startOf('day').subtract(7 * periodsToSubtract, 'day');
      toDate = fromDate.clone().add(7, 'day').subtract(1, 'ms');
      break;
    case '30daysago':
      fromDate = now.startOf('day').subtract(30 * periodsToSubtract, 'day');
      toDate = fromDate.clone().add(30, 'day').subtract(1, 'ms');
      break;
    case '60daysago':
      fromDate = now.startOf('day').subtract(60 * periodsToSubtract, 'day');
      toDate = fromDate.clone().add(60, 'day').subtract(1, 'ms');
      break;
    case '90daysago':
      fromDate = now.startOf('day').subtract(90 * periodsToSubtract, 'day');
      toDate = fromDate.clone().add(90, 'day').subtract(1, 'ms');
      break;
    case '120daysago':
      fromDate = now.startOf('day').subtract(120 * periodsToSubtract, 'day');
      toDate = fromDate.clone().add(120, 'day').subtract(1, 'ms');
      break;
    case '180daysago':
      fromDate = now.startOf('day').subtract(180 * periodsToSubtract, 'day');
      toDate = fromDate.clone().add(180, 'day').subtract(1, 'ms');
      break;
    case 'week': // kept for compatibility reasons
    case 'month': // kept for compatibility reasons
    case 'quarter': // kept for compatibility reasons
    case 'year': // kept for compatibility reasons
      fromDate = now.startOf('day').subtract(periodsToSubtract, period);
      toDate = fromDate.clone().add(1, period).subtract(1, 'ms');
      break;
    default:
      fromDate = now;
      toDate = now;
      break;
  }

  return [
    fromDate.toISOString(),
    toDate.toISOString(),
  ];
};
