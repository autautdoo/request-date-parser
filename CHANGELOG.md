# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## Unreleased

## 3.1.1
### Changed
- IGPIMP-2996 - Fixed period calculations for `toDate` when flag `previous` is set to true

## 3.1.0
### Added
- Support `all` option as period

## 3.0.0
### Changed
- Updated dependencies
### Added
- IGPIMP-1785 - Support `last60days` and `60daysago` periods

## 2.0.0
### Changed
- IGPIMP-1672 - `getFromTo` now returns array [`fromDate`,`toDate`] instead previous object

## 1.3.0
### Changed
- If `toDate` is sent as invalid date, falling back to end of today's date

## 1.2.0
### Changed
- `fromToHandler` now throws error if the `fromDate`/`toDate` is not correctly specified or both are missing

## 1.1.0
### Changed
- IGPSUP-1500 - changed handling for lastxdays and xtodage periods, added xdaysago period support

## 1.0.1
### Changed
- Fixed calculations for date when flag "previous" is set to true

## 1.0.0
### Added
- New periods
### Changed
- week/month/quarter/year period options now correspond to weekToDate/monthToDate/quarterToDate/yearToDate respectively

## 0.2.0
### Added
- pipelines ([81bf99c](https://bitbucket.org/autautdoo/request-date-parser/commits/81bf99c))
